<?php

namespace Roots\Sage\Helper;

/**
 * Theme View
 *
 * Include a file and(optionally) pass arguments to it.
 *
 * @param string $file The file path, relative to theme root
 * @param array $args The arguments to pass to this file. Optional.
 * Default empty array.
 *
 * @return object Use render() method to display the content.
 */
if (!class_exists('Template')) {
    class Template
    {
        private $args;
        private $file;

        public function __get($name)
        {
            return $this->args[$name];
        }

        public function __construct($slug, $name = null, $args = array())
        {
            $file = $this->__file($slug, $name);
            $this->file = $file;
            $this->args = $args;
        }

        public function __isset($name)
        {
            return isset($this->args[$name]);
        }

        public function __file($slug, $name)
        {
            $extension = '.php';
            $extended = false;

            if (empty($name))
                $file = $slug . $extension;
            else {
                $file = $slug . '-' . $name . $extension;
                $extended = true;
            }

            if (!file_exists(TEMPLATEPATH . '/' . $file)) {
                if ($extended) {
                    $file = $slug . $extension;
                }

                if (!file_exists(TEMPLATEPATH . '/' . $file))
                    $file = false;

            }

            return $file;

        }

        public function render()
        {
            if (locate_template($this->file)) {
                //Theme Check free. Child themes support.
                return locate_template($this->file);

            }
        }
    }
}


/**
 * Sage Get Template Part
 *
 * An alternative to the native WP function `get_template_part`
 *
 * @see PHP class Template
 * @param string $file The file path, relative to theme root
 * @param array $args The arguments to pass to this file. Optional.
 * Default empty array.
 *
 * @return string The HTML from $file
 */
if (!function_exists('sage_get_template_part')) {
    function sage_get_template_part($slug, $name = null, $args = array())
    {
        $template = new Template($slug, $name, $args);
        return $template->render();
    }
}
