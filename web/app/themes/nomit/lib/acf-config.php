<?php

namespace Roots\Sage\Acf;

    /**
     * ACF Settings
     *
     * @link http://www.advancedcustomfields.com/resources/local-json/
     * @link http://www.advancedcustomfields.com/resources/acfsettings/
     * @link http://www.advancedcustomfields.com/resources/including-acf-in-a-plugin-theme/
     */

/**
 * Changes the ACF default settings path
 *
 * @param $path ACF settings path.
 * @return string
 */
function sage_acf_settings_path($path)
{

// update path
    $path = get_template_directory() . '/extensions/acf/';
// return
    return $path;
}

//add_filter('acf/settings/path', __NAMESPACE__ . '\\sage_acf_settings_path');

/**
 * Changes the ACF default settings dir
 *
 * @param $dir ACF settings dir.
 * @return string
 */
//add_filter('acf/settings/dir', __NAMESPACE__ . '\\sage_acf_settings_dir');

function sage_acf_settings_dir($dir)
{

// update path
    $dir = get_template_directory_uri() . '/extensions/acf/';
// return
    return $dir;
}

/**
 * Changes the ACF JSON default  path
 *
 * @param $path ACF JSON path.
 * @return string
 */
add_filter('acf/settings/save_json', __NAMESPACE__ . '\\sage_acf_json_save_point');

function sage_acf_json_save_point($path)
{

    // update path
    $path = get_template_directory() . '/fields/acf-json';


    // return
    return $path;

}

/**
 * Updates the ACF JSON loading paths
 *
 * @param $paths ACF JSON path.
 * @return array
 */
add_filter('acf/settings/load_json', __NAMESPACE__ . '\\sage_acf_json_load_point');

function sage_acf_json_load_point($paths)
{

    // append path
    $paths[] = get_template_directory() . '/fields/acf-json';

    // return
    return $paths;

}

/**
 * Disables the ACF Interface
 *
 */
//add_filter('acf/settings/show_admin', '__return_false');