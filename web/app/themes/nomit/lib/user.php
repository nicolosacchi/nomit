<?php
/**
 * User Helper Class
 *
 */

namespace Roots\Sage\Helper;

if (!class_exists('userHelper')):
    class userHelper
    {
        /**
         * Grab current user info
         * @return bool|mixed|null|string|void
         */
        public static function getUserTier()
        {
            $user_info = wp_get_current_user();
            $price_tier = $user_info->ID ? get_field('user_tier', 'user_' . $user_info->ID) : "General";

            return $price_tier;
        }
    }
endif;