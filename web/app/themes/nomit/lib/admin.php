<?php

namespace Roots\Sage\Admin;

use Roots\Sage\Assets;

/**
 * Admin Utility functions
 *
 */

/**
 * Creates a new Option Page
 */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug' => 'theme_options',
        'capability' => 'edit_posts',
        'position' => '99.2',
        'icon_url' => 'dashicons-admin-site',
        'redirect' => true

    ));

}

/**
 * Updates Theme Options after theme is activated
 */
function sage_theme_setup()
{

    //update field disable_logo
    sage_update_field('disable_logo', 0);

    //update field update_logo
    sage_update_field('update_logo', 0);

    //update field upload_logo
    $logo_url = get_template_directory_uri() . '/images/admin/admin-logo.png';
    sage_update_field('upload_logo', $logo_url);

    //update field update_favicon
    sage_update_field('update_favicon', 0);

    //update field upload_favicon
    $upload_favicon = get_template_directory_uri() . '/images/favicon.ico';
    sage_update_field('upload_favicon', $upload_favicon);

    //update field disable_icon
    sage_update_field('disable_icon', 0);

    //update field update_icon
    sage_update_field('update_icon', 0);

    //update field upload_icon
    $upload_icon = Assets\asset_path('images/ignite-logo-white.png');
    sage_update_field('upload_icon', $upload_icon);

    //update field enable_about_menu
    sage_update_field('enable_about_menu', 0);

    //update field enable_wordpress_menu
    sage_update_field('enable_wordpress_menu', 0);

    //update field enable_doc_menu
    sage_update_field('enable_doc_menu', 0);

    //update field enable_forums_menu
    sage_update_field('enable_forums_menu', 0);

    //update field enable_feedback_menu
    sage_update_field('enable_feedback_menu', 0);

    //update field enable_site_menu
    sage_update_field('enable_site_menu', 0);

    //update field enable_top_comments_menu
    sage_update_field('enable_top_comments_menu', 0);

    //update field enable_new_menu
    sage_update_field('enable_new_menu', 0);

    //update field enable_updates_menu
    sage_update_field('enable_updates_menu', 0);

    //update field enable_posts_menu
    sage_update_field('enable_posts_menu', 1);

    //update field enable_media_menu
    sage_update_field('enable_media_menu', 1);

    //update field enable_pages_menu
    sage_update_field('enable_pages_menu', 1);

    //update field enable_comments_menu
    sage_update_field('enable_comments_menu', 0);

    //update field enable_yoast_menu
    sage_update_field('enable_yoast_menu', 0);

    //update field enable_appearance_menu
    sage_update_field('enable_appearance_menu', 1);

    //update field enable_plugins_menu
    sage_update_field('enable_plugins_menu', 1);

    //update field enable_users_menu
    sage_update_field('enable_users_menu', 1);

    //update field enable_tools_menu
    sage_update_field('enable_tools_menu', 1);

    //update field enable_settings_menu
    sage_update_field('enable_settings_menu', 1);

    //update field enable_wp_dashboard
    sage_update_field('enable_wp_dashboard', 0);

    //update field enable_wp_rightnow
    sage_update_field('enable_wp_rightnow', 0);

    //update field enable_wp_quickdraft
    sage_update_field('enable_wp_quickdraft', 0);

    //update field enable_wp_activity
    sage_update_field('enable_wp_activity', 0);

    //update field enable_wp_wordpressnews
    sage_update_field('enable_wp_wordpressnews', 0);

    //update field disable_controlpanel
    sage_update_field('disable_controlpanel', 0);

    //update field control_panel_title
    $cp_title = 'Welcome to your Control Panel';
    sage_update_field('control_panel_title', $cp_title);

    //update field control_panel_message
    $cp_message = '<p>Welcome to your Control Panel. You can use this control panel to manage every aspect of your App. </p>'
        . '<p>Take a moment to familiarise yourself with the layout. Pay particular attention to the main menu options on the left of the page .'
        . '<br>Each of these options contains a sub-menu giving you easy access to every management tool at your disposal.</p>';
    sage_update_field('control_panel_message', $cp_message);

    //update field disable_technicalinfo
    sage_update_field('disable_technicalinfo', 0);

    //update field release_date
    sage_update_field('release_date', date('F j, Y'));

    //update field latest_date
    sage_update_field('latest_date', date('F j, Y'));

    $sage_theme = wp_get_theme();

    //update field theme_name
    sage_update_field('theme_name', $sage_theme->get('Name'));

    //update field theme_version
    sage_update_field('theme_version', $sage_theme->get('Version'));

    //update field disable_rss
    sage_update_field('disable_rss', 0);

    //update field rss_title
    $rss_title = 'Recently on Ignite Online';
    sage_update_field('rss_title', $rss_title);

    //update field rss_feed
    $rss_feed = 'http://yoursite.com.au/feed/';
    sage_update_field('rss_feed', $rss_feed);

    //update field disable_support
    sage_update_field('disable_support', 0);

    //update field support_logo
    $support_logo = Assets\asset_path('images/admin/ignite-logo.png');
    sage_update_field('support_logo', $support_logo);

    //update field support_url
    $support_url = 'http://igniteonline.com.au/contact/';
    sage_update_field('support_url', $support_url);

    //update field support_message
    $support_message = '<p>Send us a message for any issue that you have been facing and you are unable to solve. </br> '
        . 'Please <a href="http://igniteonline.com.au/contact/" target="_blank">say Hello</a> at any time and we may '
        . 'help you to solve it.</p>';
    sage_update_field('support_message', $support_message);

    //update field disable_powered_by
    sage_update_field('disable_powered_by', 0);

    //update field update_powered_by
    $update_powered_by = 'Ignite Online';
    sage_update_field('update_powered_by', $update_powered_by);

    //update field disable_wordpress_version
    sage_update_field('disable_wordpress_version', 0);


}

add_action('after_setup_theme', __NAMESPACE__ . '\\sage_theme_setup');

/**
 * Updates the field with the parameter
 *
 * @param string $field_name Field Name.
 * @param mixed $param Params.
 */
function sage_update_field($field_name, $param)
{

    if (function_exists('get_field')) {
        $value = get_field($field_name, 'option');

        if (is_null($value) || empty($value)) {
            update_field($field_name, $param, 'option');
        }
    }

}

/**
 * Get field value or return FALSE if fails.
 *
 * @param string $field_name Field Name.
 * @return mixed
 */
function sage_get_field($field_name)
{

    if (function_exists('get_field')) {

        $value = get_field($field_name, 'option');

        if (is_null($value) || empty($value)) {
            return false;
        }

        return $value;

    }

    return false;
}

/**
 * Disables the Admin Bar in the front-end area.
 */
add_filter('show_admin_bar', '__return_false');


/**
 * Inserts a custom Admin WordPress CSS stylesheet.
 */
function sage_custom_admin_css()
{
    wp_enqueue_style('admin_css', Assets\asset_path('styles/admin/admin.css'));
}

add_action('admin_head', __NAMESPACE__ . '\\sage_custom_admin_css');

/**
 * Inserts a custom Login WordPress CSS stylesheet.
 */
function sage_custom_login_css()
{
    // get theme options
    $disable_logo = sage_get_field('disable_logo');
    $update_logo = sage_get_field('update_logo');

    if (!$disable_logo) {
        wp_enqueue_style('login_css', Assets\asset_path('styles/admin/login.css'));

        if ($update_logo) {
            $upload_logo = sage_get_field('upload_logo');

            if ($upload_logo) {
                ?>
                <style type="text/css">
                    .login h1 a {
                        background-image: none, url(<?php _e($upload_logo) ?>) !important;
                    }
                </style>
                <?php

            }

        }
    }


}

add_action('login_head', __NAMESPACE__ . '\\sage_custom_login_css');


/**
 * Changes link url Admin WordPress Logo.
 */
function sage_login_logo_custom_link()
{
    return admin_url();
}

add_filter('login_headerurl', __NAMESPACE__ . '\\sage_login_logo_custom_link');


/**
 * Changes the Admin WordPress Logo tooltip.
 */
function sage_change_title_on_logo()
{
    // get theme options
    $disable_powered_by = sage_get_field('disable_powered_by');
    $update_powered_by = sage_get_field('update_powered_by');

    if (!$disable_powered_by) return 'Powered by ' . $update_powered_by;


}

add_filter('login_headertitle', __NAMESPACE__ . '\\sage_change_title_on_logo');


/**
 * Changes Howdy Message.
 */
add_action('admin_bar_menu', __NAMESPACE__ . '\\sage_custom_account_menu', 11);

function sage_custom_account_menu($wp_admin_bar)
{
    $user_id = get_current_user_id();
    $current_user = wp_get_current_user();
    $profile_url = get_edit_profile_url($user_id);

    if (0 != $user_id) {
        /* Add the "My Account" menu */
        $avatar = get_avatar($user_id, 28);
        $howdy = sprintf(__('G\'day, %1$s'), $current_user->display_name);
        $class = empty($avatar) ? '' : 'with-avatar';

        $wp_admin_bar->add_menu(array(
            'id' => 'my-account',
            'parent' => 'top-secondary',
            'title' => $howdy . $avatar,
            'href' => $profile_url,
            'meta' => array(
                'class' => $class,
            ),
        ));

    }
}


/**
 * Changes the Admin WordPress Footer.
 */
function sage_custom_admin_footer()
{
    // get theme options
    $disable_powered_by = sage_get_field('disable_powered_by');
    $update_powered_by = sage_get_field('update_powered_by');

    if (!$disable_powered_by) _e('<span id="footer-thankyou">Powered by ' . $update_powered_by . '</span>', 'sage');
}

add_filter('admin_footer_text', __NAMESPACE__ . '\\sage_custom_admin_footer');


/**
 * Updates version on Admin WordPress Footer.
 */
function sage_update_footer_version($version)
{
    // get theme options
    $disable_wordpress_version = sage_get_field('disable_wordpress_version');
    if (!$disable_wordpress_version) return 'WordPress ' . $version;
}

add_filter('update_footer', __NAMESPACE__ . '\\sage_update_footer_version', 9999);


/**
 * Updates WordPress Dashboard CSS stylesheet.
 */
function sage_custom_dashboard_css()
{
    // get theme options
    $enable_wp_dashboard = sage_get_field('enable_wp_dashboard');
    if (!$enable_wp_dashboard) wp_enqueue_style('dashboard_css', Assets\asset_path('styles/admin/dashboard.css'));

}

add_action('admin_head', __NAMESPACE__ . '\\sage_custom_dashboard_css');

/**
 * Adds a widget in WordPress Dashboard.
 */
function sage_technical_dashboard_widget_function()
{
    // get theme options
    $release_date = sage_get_field('release_date');
    $latest_date = sage_get_field('latest_date');
    $theme_name = sage_get_field('theme_name');
    $theme_version = sage_get_field('theme_version');

    // Entering the text between the quotes
    ?>
    <ul>
        <li><strong>Release Date</strong>: <?php _e($release_date); ?></li>
        <li><strong>Latest Update Date</strong>: <?php _e($latest_date); ?></li>
        <li><strong>Theme</strong>: <?php _e($theme_name); ?></li>
        <li><strong>Version</strong>: <?php _e($theme_version); ?></li>
    </ul>
    <?php
}


/**
 * Inserts RSS Dashboard Widget.
 */
function sage_rss_dashboard_widget()
{
    // get theme options
    $rss_feed = sage_get_field('rss_feed');

    if ($rss_feed) {
        if (function_exists('fetch_feed')) {
            include_once(ABSPATH . WPINC . '/feed.php'); // include the required file
            $feed = fetch_feed($rss_feed); // specify the source feed
            $limit = 0;

            if (!is_wp_error($feed)) {
                $limit = $feed->get_item_quantity(7); // specify number of items
                $items = $feed->get_items(0, $limit); // create an array of items
            }


        }
        if ($limit == 0) echo '<div>The RSS Feed is either empty or unavailable.</div>'; // fallback message
        else foreach ($items as $item) {
            ?>

            <h4 style="margin-bottom: 0;">
                <a href="<?php echo $item->get_permalink(); ?>"
                   title="<?php echo mysql2date(__('j F Y @ g:i a', ''), $item->get_date('Y-m-d H:i:s')); ?>"
                   target="_blank">
                    <?php echo $item->get_title(); ?>
                </a>
            </h4>
            <p style="margin-top: 0.5em;">
                <?php echo substr($item->get_description(), 0, 200); ?>
            </p>
            <?php
        }

    }

}


/**
 * Inserts Support Dashboard Widget.
 */
function sage_support_widget_function()
{
    // get theme options
    $support_logo = sage_get_field('support_logo');
    $support_url = sage_get_field('support_url');
    $support_message = sage_get_field('support_message');

    $print_logo = ($support_logo) ? $support_logo : Assets\asset_path('images/admin/ignite-icon.png');

    ?>
    <div>
        <a href="<?php _e($support_url); ?>" target="_blank" class="wp_support_widget_logo">
            <img src="<?php _e($print_logo); ?>" alt="Support Service" height="70">
        </a>
        <?php _e($support_message); ?>
    </div>
    <div class="clear"></div>
    <?php
}

/**
 * Inserts Welcome Dashboard Widget.
 */
function sage_dashboard_widget_function()
{
    //get theme options
    $disable_logo = sage_get_field('disable_logo');
    $update_logo = sage_get_field('update_logo');
    $upload_logo = sage_get_field('upload_logo');
    $control_panel_message = sage_get_field('control_panel_message');
    $admin_logo = Assets\asset_path('images/admin/ignite-logo.png');
    $print_logo = ($disable_logo) ?
        get_admin_url() . '/images/wordpress-logo.png' :
        (($update_logo) ? (($upload_logo) ? $upload_logo : $admin_logo) : $admin_logo);
    $logo_height = ($disable_logo) ? '63' : '75';

    // Entering the text between the quotes
    ?>
    <div class="admin-logo"><img src="<?php _e($print_logo); ?>" alt="<?php _e(get_bloginfo('name')); ?>"
                                 height="<?php _e($logo_height); ?>"/></div>

    <?php if ($control_panel_message): _e($control_panel_message) ?>
<?php else: ?>
    <p>Welcome to your Control Panel. You can use this control panel to manage every aspect of your App. </p>
    <p>Take a moment to familiarise yourself with the layout. Pay particular attention to the main menu options on the
        left of the page . <br/>Each of these options contains a sub-menu giving you easy access to every management
        tool at your disposal.
    </p>
<?php endif; ?>
    <div class="clear"></div>
    <?php
}


/**
 * Calls all custom dashboard widgets
 */
function sage_add_dashboard_widgets()
{
    // get theme options
    /*
    Be sure to drop any other created Dashboard Widgets
    in this function and they will all load.
    */


}

add_action('wp_dashboard_setup', __NAMESPACE__ . '\\sage_add_dashboard_widgets');


/**
 * Disables default Admin WordPress widgets.
 */
function sage_disable_default_dashboard_widgets()
{
    // get theme options
    $enable_wp_wordpressnews = sage_get_field('enable_wp_wordpressnews');
    $enable_wp_rightnow = sage_get_field('enable_wp_rightnow');
    $enable_wp_quickdraft = sage_get_field('enable_wp_quickdraft');
    $enable_wp_activity = sage_get_field('enable_wp_activity');

    // removes widgets is selected
    if (!$enable_wp_rightnow) remove_meta_box('dashboard_right_now', 'dashboard', 'core'); // Right Now Widget / At a Glance
    if (!$enable_wp_quickdraft) remove_meta_box('dashboard_quick_press', 'dashboard', 'core'); // Quick Press Widget / Quick Draft
    if (!$enable_wp_wordpressnews) remove_meta_box('dashboard_primary', 'dashboard', 'core'); // WordPress News
    if (!$enable_wp_activity) remove_meta_box('dashboard_activity', 'dashboard', 'core'); // Activity


    // Remove Previous versions widgets
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'core'); // Incoming Links Widget
    remove_meta_box('dashboard_plugins', 'dashboard', 'core'); // Plugins Widget
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core'); // Recent Drafts Widget
    remove_meta_box('dashboard_secondary', 'dashboard', 'core'); //

    // removing plugin dashboard boxes
    remove_meta_box('yoast_db_widget', 'dashboard', 'normal'); // Yoast's SEO Plugin Widget

    /*
    have more plugin widgets you'd like to remove?
    share them with us so we can get a list of
    the most commonly used. :D
    https://github.com/eddiemachado/bones/issues
    */
}

add_action('admin_menu', __NAMESPACE__ . '\\sage_disable_default_dashboard_widgets');
remove_action('welcome_panel', 'wp_welcome_panel');


/**
 * Removes metaboxes from Post & Pages
 */
function sage_customise_meta_boxes()
{
    /* Removes meta boxes from Posts */
    remove_meta_box('postcustom', 'post', 'normal');
    remove_meta_box('trackbacksdiv', 'post', 'normal');
    remove_meta_box('commentstatusdiv', 'post', 'normal');
    remove_meta_box('commentsdiv', 'post', 'normal');
    //remove_meta_box('tagsdiv-post_tag', 'post', 'normal');
    remove_meta_box('postexcerpt', 'post', 'normal');
    /* Removes meta boxes from pages */
    remove_meta_box('postcustom', 'page', 'normal');
    remove_meta_box('trackbacksdiv', 'page', 'normal');
    remove_meta_box('revisionsdiv', 'page', 'normal');
    remove_meta_box('commentstatusdiv', 'page', 'normal');
    remove_meta_box('commentsdiv', 'page', 'normal');
}

add_action('admin_init', __NAMESPACE__ . '\\sage_customise_meta_boxes');


/**
 * Hides unused menu items
 */
function sage_hide_menu_items()
{
    global $menu;

    // get theme options
    $enable_posts_menu = sage_get_field('enable_posts_menu');
    $enable_media_menu = sage_get_field('enable_media_menu');
    $enable_pages_menu = sage_get_field('enable_pages_menu');
    $enable_comments_menu = sage_get_field('enable_comments_menu');
    $enable_appearance_menu = sage_get_field('enable_appearance_menu');
    $enable_plugins_menu = sage_get_field('enable_plugins_menu');
    $enable_users_menu = sage_get_field('enable_users_menu');
    $enable_tools_menu = sage_get_field('enable_tools_menu');
    $enable_settings_menu = sage_get_field('enable_settings_menu');

    $restricted = array();

    if (!$enable_posts_menu) array_push($restricted, __('Posts'));
    if (!$enable_media_menu) array_push($restricted, __('Media'));
    if (!$enable_pages_menu) array_push($restricted, __('Pages'));
    if (!$enable_comments_menu) array_push($restricted, __('Comments'));
    if (!$enable_appearance_menu) array_push($restricted, __('Appearance'));
    if (!$enable_plugins_menu) array_push($restricted, __('Plugins'));
    if (!$enable_users_menu) array_push($restricted, __('Users'));
    if (!$enable_tools_menu) array_push($restricted, __('Tools'));
    if (!$enable_settings_menu) array_push($restricted, __('Settings'));

    end($menu);


    while (prev($menu)) {
        $value = explode(' ', $menu[key($menu)][0]);
        if (in_array($value[0] != NULL ? $value[0] : "", $restricted)) {
            unset($menu[key($menu)]);
        }
    }
}

add_action('admin_menu', __NAMESPACE__ . '\\sage_hide_menu_items');


/**
 * Removes submenus
 */
function sage_remove_submenus()
{
    global $submenu;
    /*
    unset($submenu['index.php'][10]); // Removes 'Updates'.
    unset($submenu['themes.php'][5]); // Removes 'Themes'.
    unset($submenu['options-general.php'][15]); // Removes 'Writing'.
    unset($submenu['options-general.php'][25]); // Removes 'Discussion'.
    unset($submenu['edit.php'][16]); // Removes 'Tags'.
    */
}

add_action('admin_menu', __NAMESPACE__ . '\\sage_remove_submenus');


/**
 * Removes Updates submenu
 */
function sage_remove_updates()
{

    // get theme options
    $enable_updates_menu = sage_get_field('enable_updates_menu');

    if (!$enable_updates_menu) remove_submenu_page('index.php', 'update-core.php');
}

add_action('admin_init', __NAMESPACE__ . '\\sage_remove_updates');

/**
 * Inserts a custom Admin WordPress CSS stylesheet.
 */
function sage_custom_admin_icon_css()
{
    // get theme options
    $disable_icon = sage_get_field('disable_icon');
    $update_icon = sage_get_field('update_icon');

    var_dump($disable_icon);

    if (!$disable_icon) {
        wp_enqueue_style('admin_icon_css', Assets\asset_path('styles/admin/icons.css'));

        if ($update_icon) {
            $upload_icon = sage_get_field('upload_icon');

            if ($upload_icon) {
                ?>
                <style type="text/css">
                    #wpadminbar #wp-admin-bar-wp-logo > .ab-item {
                        background: url(<?php _e($upload_icon) ?>) 10px 0 no-repeat !important;
                        background-size: contain;
                    }
                </style>
                <?php

            }

        }
    } else {
        wp_dequeue_style('admin_icon_css', Assets\asset_path('styles/admin/icons.css'));
    }
}

add_action('admin_head', __NAMESPACE__ . '\\sage_custom_admin_icon_css');


/**
 * Changes the Admin Toolbar
 */
function sage_change_toolbar($wp_admin_bar)
{
    // get theme options
    $enable_about_menu = sage_get_field('enable_about_menu');
    $enable_wordpress_menu = sage_get_field('enable_wordpress_menu');
    $enable_doc_menu = sage_get_field('enable_doc_menu');
    $enable_forums_menu = sage_get_field('enable_forums_menu');
    $enable_feedback_menu = sage_get_field('enable_feedback_menu');
    $enable_site_menu = sage_get_field('enable_site_menu');
    $enable_top_comments_menu = sage_get_field('enable_top_comments_menu');
    $enable_new_menu = sage_get_field('enable_new_menu');
    $enable_updates_menu = sage_get_field('enable_updates_menu');
    $enable_yoast_menu = sage_get_field('enable_yoast_menu');

    // disable admin toolbar options at init
    //$wp_admin_bar->remove_node('wp-logo');

    if (!$enable_about_menu) $wp_admin_bar->remove_menu('about'); // Remove the about WordPress link
    if (!$enable_wordpress_menu) $wp_admin_bar->remove_menu('wporg'); // Remove the WordPress.org link
    if (!$enable_doc_menu) $wp_admin_bar->remove_menu('documentation'); // Remove the WordPress documentation link
    if (!$enable_forums_menu) $wp_admin_bar->remove_menu('support-forums'); // Remove the support forums link
    if (!$enable_feedback_menu) $wp_admin_bar->remove_menu('feedback'); // Remove the feedback link
    if (!$enable_top_comments_menu) $wp_admin_bar->remove_node('comments');
    if (!$enable_site_menu) $wp_admin_bar->remove_node('site-name');
    if (!$enable_new_menu) $wp_admin_bar->remove_node('new-content');
    if (!$enable_updates_menu) $wp_admin_bar->remove_node('updates');
    if (!$enable_yoast_menu) $wp_admin_bar->remove_node('wpseo-menu');

    // update logo url
    if (!$enable_about_menu) {
        sage_edit_admin_bar(array('wp-logo'), 'href', get_admin_url());
        sage_edit_admin_bar(array('wp-logo'), 'meta', array('title' => get_bloginfo('name')));
    }


}

add_action('admin_bar_menu', __NAMESPACE__ . '\\sage_change_toolbar', 999);


/**
 * Edit the Admin Toolbar Function
 */
if (!function_exists('sage_edit_admin_bar')) {
    function sage_edit_admin_bar($id, $property, $value)
    {
        global $wp_admin_bar;

        if (!is_array($id)) {
            $id = array($id);
        }

        $all_nodes = $wp_admin_bar->get_nodes();

        foreach ($all_nodes as $key => $val) {
            $current_node = $all_nodes[$key];
            $wp_admin_bar->remove_node($key);

            if (in_array($key, $id)) {
                $current_node->$property = $value;
            }

            $wp_admin_bar->add_node($current_node);
        }
    }
}


/**
 * Update Admin Favicon
 */
function sage_admin_area_favicon()
{
    // get theme options
    $update_favicon = sage_get_field('update_favicon');
    $upload_favicon = sage_get_field('upload_favicon');
    $favicon = Assets\asset_path('images/icons/favicon.ico');

    $favicon_url = (!$update_favicon) ?
        $favicon : (($upload_favicon) ? $upload_favicon : $favicon);
    _e('<link rel="shortcut icon" href="' . $favicon_url . '" />');
}

add_action('admin_head', __NAMESPACE__ . '\\sage_admin_area_favicon');
add_action('login_head', __NAMESPACE__ . '\\sage_admin_area_favicon');