<?php

namespace Roots\Sage\Helper;

use Roots\Sage\Utils;
use Roots\Sage\Assets;

if (!class_exists('Theme')) :

    class Theme
    {

        /**
         *  __construct
         *
         *  A dummy constructor to ensure Ignite Theme is only initialized once
         *
         * @type    function
         * @date    23/11/15
         * @since    0.1.2
         *
         */

        function __construct()
        {
            // Constructor methods

        }

        /**
         *  __()
         *
         * echo template helper function
         *
         * @param $method
         * @param null $args
         * @param bool|true $echo
         * @return bool
         */
        public function __($method, $args = null, $echo = true)
        {

            if (!empty($method)) {

                if (!empty($args)) {
                    $function = $this->$method($args);

                } else {
                    $function = $this->$method();
                }

                if (method_exists($this, $method) && (is_callable(array($this, $method))))

                    if ($echo)
                        echo $function;
                    else
                        return $function;

            }

            if ($echo)
                echo '';
            else
                return false;
        }

        /**
         * asset_path
         *
         * @param $filename
         * @return string
         */
        public function asset_path($filename)
        {
            $dist_path = get_template_directory_uri() . DIST_DIR;
            $directory = dirname($filename) . '/';
            $file = basename($filename);

            return $dist_path . $directory . $file;
        }

        /**
         * get_the_excerpt
         *
         * @param $text
         * @param $excerpt
         * @return mixed|void
         */
        public function get_the_excerpt($text, $excerpt)
        {
            if ($excerpt) return $excerpt;

            $text = strip_shortcodes($text);

            $text = apply_filters('the_content', $text);
            $text = str_replace(']]>', ']]&gt;', $text);
            $text = strip_tags($text);
            $excerpt_length = apply_filters('excerpt_length', 55);
            // $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
            $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
            if (count($words) > $excerpt_length) {
                array_pop($words);
                $text = implode(' ', $words);
                // $text = $text . $excerpt_more;
            } else {
                $text = implode(' ', $words);
            }

            return apply_filters('wp_trim_excerpt', $text, $excerpt);
        }

        /**
         * get_the_slug
         *
         * Get the Slug of posts
         *
         * @param null $id
         * @return string
         */
        public function get_the_slug($id = null)
        {
            if (empty($id)):
                global $post;
                if (empty($post))
                    return ''; // No global $post var available.
                $id = $post->ID;
            endif;

            $slug = basename(get_permalink($id));
            return $slug;
        }

        /**
         * url_to_link
         *
         * @param $s
         * @return mixed
         */
        public function url_to_link($s)
        {

            $response = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a target="blank" rel="nofollow" href="$1" target="_blank">$1</a>', $s);
            return $response;
        }


        /**
         * get_string_between
         *
         * Get substring from two strings
         *
         * @param $string
         * @param $start
         * @param $end
         * @return string
         */
        public function get_string_between($string, $start, $end)
        {
            $string = " " . $string;
            $ini = strpos($string, $start);
            if ($ini == 0) return "";
            $ini += strlen($start);
            $len = strpos($string, $end, $ini) - $ini;
            return substr($string, $ini, $len);
        }

        /**
         * encode_email
         *
         * Encode email addresses
         *
         * @param $e
         * @return string
         */
        public function encode_email($e)
        {
            $output = '';
            for ($i = 0; $i < strlen($e); $i++) {
                $output .= '&#' . ord($e[$i]) . ';';
            }
            return $output;
        }


    }

    /**
     * theme
     *
     *  The main function responsible for returning the one true theme Instance to functions everywhere.
     *  Use this function like you would a global variable, except without needing to declare the global.
     *
     *  Example: <?php $theme = theme(); ?>
     *
     * @type    function
     * @date    23/11/15
     * @since    0.1.2
     *
     * @return Theme|string|\WP_Theme
     */
    function theme()
    {

        global $theme;

        if (!isset($theme)) {

            $theme = new Theme();

        }

        return $theme;
    }


    // initialize
    theme();

endif; // class_exists check

