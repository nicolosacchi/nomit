<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (file_exists(get_template_directory() . "/favicon.html")): echo file_get_contents(get_template_directory() . "/favicon.html"); endif; ?>
    <?php wp_head(); ?>
</head>