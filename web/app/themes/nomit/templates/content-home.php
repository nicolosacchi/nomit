<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <?php the_content(); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <img src="<?= wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>" alt="home"
                 class="img-responsive">
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <!-- projects carousel -->
            <!-- /.projects carousel -->
        </div>
        <div class="col-xs-12 col-sm-6">
            <h1>Progetti</h1>
            <p>nomit é in continua evoluzione, sempre con nuovi progetti.</p>
            <p>Mibh vulputate cursus a sit amet mauris. Morbi accumsan ipsuma ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.
                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
            <a href="">Vai ai progetti</a>
        </div>
    </div>
</div>

<div class="container-fluid blog-section">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>Blog</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6"></div>
                <div class="col-xs-12 col-sm-6"></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid sostienici">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h1>Sostienici</h1>
                    <p>nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                    <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <img src="" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>
