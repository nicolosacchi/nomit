<?php
use Roots\Sage\Assets;
?>
<header class="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-md-2">
                <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                    <img src="<?= Assets\asset_path("images/logo.png") ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-6 col-md-10">
                <nav class="nav-primary">
                    <?php
                    if (has_nav_menu('primary_navigation')) :
                        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
                    endif;
                    ?>
                </nav>
            </div>
        </div>
    </div>
</header>
