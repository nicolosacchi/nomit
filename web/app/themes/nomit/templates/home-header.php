<?php $slides = get_field("slides", get_the_ID()); ?>
<?php if (is_array($slides)): ?>
    <!-- Carousel -->

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php foreach ($slides as $slide): ?>
                <div class="item active">
                    <img src="<?= $slide['image'] ?>" alt="slide" class="img-responsive">
                    <div class="carousel-caption">
                        <p><?= !empty($slide['caption']) ? $slide['caption'] : get_the_title($slide['page_link']) ?></p>
                        <p><a href="<?= $slide['page_link'] ?>">READ MORE</a></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
    </div>

    <!--/. Carousel -->
<?php endif; ?>