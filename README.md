# [WP New Project | Ignite Online ](http://igniteproject.ignitestaging.com.au/)

WP New Project is a complete package starter for new WordPress projects (internal use) based on [Bedrock](https://roots.io/bedrock/) a modern WordPress stack with the best development tools and project structure, and [Sage](https://roots.io/sage/) a WordPress starter theme based on HTML5 Boilerplate, gulp, Bower, and Bootstrap Sass.

This package contains customised functions and extended plugins that are not available on Bedrock or Sage. The package will be updated automatically and it is strongly recommended to start any new project from an instance of this repository. This repository is not intended to be updated by the developers, so please do not try to commit any change on this repository.

## Features
### From Bedrock
* Better folder structure
* Dependency management with [Composer](http://getcomposer.org)
* Easy WordPress configuration with environment specific files
* Environment variables with [Dotenv](https://github.com/vlucas/phpdotenv)
* Autoloader for mu-plugins (use regular plugins as mu-plugins)

### From Sage
* [gulp](http://gulpjs.com/) build script that compiles both Sass and Less, checks for JavaScript errors, optimizes images, and concatenates and minifies files
* [BrowserSync](http://www.browsersync.io/) for keeping multiple browsers and devices synchronized while testing, along with injecting updated CSS and JS into your browser while you're developing
* [Bower](http://bower.io/) for front-end package management
* [asset-builder](https://github.com/austinpray/asset-builder) for the JSON file based asset pipeline
* [Bootstrap](http://getbootstrap.com/)
* [Theme wrapper](https://roots.io/sage/docs/theme-wrapper/)
* ARIA roles and microformats
* Posts use the [hNews](http://microformats.org/wiki/hnews) microformat
* [Multilingual ready](https://roots.io/wpml/) and over 30 available [community translations](https://github.com/roots/sage-translations)

### From Custom
* [Advanced Custom Fields](http://www.advancedcustomfields.com/) plugin on its latest version 
* [Yoast SEO](https://yoast.com/) plugin on its latest version
* [WP Migrate DB PRO](https://deliciousbrains.com/wp-migrate-db-pro/) plugin on its latest version
* [Tiny MCE Advanced](https://wordpress.org/plugins/tinymce-advanced/) plugin on its latest version
* Customised Admin Area with custom controls
* Custom development structure

## Requirements

* PHP >= 5.5
* Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 5.4.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| Node.js 0.12.x  | `node -v`    | [nodejs.org](http://nodejs.org/) |
| gulp >= 3.8.10  | `gulp -v`    | `npm install -g gulp` |
| Bower >= 1.3.12 | `bower -v`   | `npm install -g bower` |

## Installation

1. Clone the git repo - `git clone https://username@bitbucket.org/igniteonline/wp_new_project.git projectname`
2. Run `composer install`
3. Copy `.env.example` to `.env` and update environment variables to a development environment:
  * `DB_NAME` - Database name
  * `DB_USER` - Database user
  * `DB_PASSWORD` - Database password
  * `DB_HOST` - Database host
  * `WP_ENV` - Set to environment (`development`, `staging`, `production`)
  * `WP_HOME` - Full URL to WordPress home (http://example.com)
  * `WP_SITEURL` - Full URL to WordPress including subdirectory (http://example.com/wp)
  * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT` - Generate with [wp-cli-dotenv-command](https://github.com/aaemnnosttv/wp-cli-dotenv-command) or from the [WordPress Salt Generator](https://api.wordpress.org/secret-key/1.1/salt/)
4. Modify theme name in `web/app/themes` called `ignitetheme`.
4. Set your site vhost document root to `/path/to/site/web/` (`/path/to/site/current/web/` if using deploys)
5. Access WP admin at `http://project.local/wp/wp-admin`

## Theme installation

It is only required to modify theme name in `web/app/themes` called `ignitetheme` to your project name and update the project details under the `styles.css` file.

## Theme setup

Edit `lib/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, post formats, and sidebars.

## Theme development

Sage uses [gulp](http://gulpjs.com/) as its build system and [Bower](http://bower.io/) to manage front-end packages.

### Install gulp and Bower

Building the theme requires [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: `npm install -g npm@latest`.

From the command line:

1. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`

You now have all the necessary dependencies to run the build process.

### Available gulp commands

* `gulp` — Compile and optimize the files in your assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp --production` — Compile assets for production (no source maps).

### Using BrowserSync

To use BrowserSync during `gulp watch` you need to update `devUrl` at the bottom of `assets/manifest.json` to reflect your local development hostname.

For example, if your local development URL is `http://project-name.dev` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://project-name.dev"
  }
...
```
If your local development URL looks like `http://localhost:8888/project-name/` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://localhost:8888/project-name/"
  }
...
```

## Theme Documentation

Sage documentation is available at [https://roots.io/sage/docs/](https://roots.io/sage/docs/).

## Deploys

Deployment process will be managed by the Team using the available tools (Bitbucket, .git, rsync). Developers will not be required to deploy on Live environments unless instructed to do so.

### Creating a new project
1. Unbind the WP New Project git repository by deleting the `.git` folder or modifying the `config` file under the `.git` folder
2. Attached to the project files the new project respository from a selected repository located at `https://username@bitbucket.org/igniteonline/project_name.git`
3. Use [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow/) and create a new `develop` branch.
4. Commit all your changes under the `develop` branch unless instructed to do a different approach.

## Documentation

WP New Project  documentation is available at [http://igniteproject.ignitestaging.com.au/docs/](http://igniteproject.ignitestaging.com.au/docs/).

## Contributing

No contributions are required from the developers as the package will be automatically updated from the Bedrock and Sage projects. If you want to contribute building new custom features to the WP New Project instance, please let us know at studio@igniteonline.com.au.
